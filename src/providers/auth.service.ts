// Angular
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

// Firebase
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';

// SweetAlert2
import swal from 'sweetalert2';

// declare var firebase;

@Injectable()
export class AuthService {

	// _auth = firebase.auth();
	currentUser = null;

	constructor(
		private afAuth: AngularFireAuth,
		private router: Router
	) { }

	loginWithPassword(data: { email: string, password: string }): Promise<any> {
		const self = this;

		return new Promise<any>((resolve, reject) => {
			self.afAuth.auth.signInWithEmailAndPassword(data.email, data.password).then((success) => {
				resolve(success);
			}, (error) => {
				reject(error);
			});
		});
	}

	createUser(data: { email: string, password: string }): Promise<any> {
		const self = this;

		return new Promise<any>((resolve, reject) => {
			self.afAuth.auth.createUserWithEmailAndPassword(data.email, data.password).then((success) => {
				resolve(success);
			}, (error) => {
				reject(error);
			});
		});
	}

	facebookLogin(): Promise<any> {
		const self = this;

		return new Promise<any>((resolve, reject) => {
			self.afAuth.auth.signInWithPopup(new firebase.auth.FacebookAuthProvider()).then((success) => {
				resolve(success);
			}, (error) => {
				switch (error.code) {
					// Caso o usuário já possua e-mail cadastrado no sistema, ele pode linkar a conta do Facebook
					case 'auth/account-exists-with-different-credential': {
						const pendingCred = error.credential;
						const email = error.email;

						swal({
							title: 'Login via facebook',
							input: 'password',
							html: '<div style="padding:10px;">O e-mail: ' + email + ' já cadastrado <br>Realize o login para linkar as contas.</div>',
							showCancelButton: true,
							inputPlaceholder: 'Senha',
							cancelButtonText: 'Cancelar',
							confirmButtonText: 'Linkar contas',
							reverseButtons: true,
							showLoaderOnConfirm: true,
							preConfirm: function (password) {
								return new Promise(function (resolve, reject) {
									self.loginWithPassword({ email: email, password: password }).then((success) => {
										resolve(success);
									}, (error) => {
										reject('Senha incorreta.');
									});
								});
							},
							allowOutsideClick: false
						}).then(function (result) {
							self.linkAccount(pendingCred).then((success) => {
								self.router.navigate(['/main']);
								swal({
									type: 'success',
									title: 'Linkar facebook',
									html: 'Contas linkadas com sucesso!'
								});
							}, (error) => {
								swal({
									type: 'error',
									title: 'Linkar facebook',
									html: 'Falha ao linkar as contas!'
								});
							});
						});
						break;
					}
					case 'auth/timeout': {
						swal('Login via facebook', 'Tempo de espera expirado, tente novamente.');
						break;
					}
				}
				reject(error);
			});
		});
	}

	linkAccount(credential): Promise<any> {
		const self = this;

		return new Promise<any>((resolve, reject) => {
			self.afAuth.auth.currentUser.linkWithCredential(credential).then((success) => {
				resolve(success);
			}, (error) => {
				reject(error);
			});
		});
	}

	logout(): Promise<any> {
		const self = this;

		return new Promise<any>((resolve, reject) => {
			self.afAuth.auth.signOut().then((success) => {
				resolve(success);
			}, function (error) {
				reject(error);
			});
		});
	}

	isLoggedIn(): Promise<any> {
		const self = this;

		return new Promise<any>((resolve, reject) => {
			const unsubscribe = self.afAuth.auth.onAuthStateChanged(function (user) {
				if (user) {
					unsubscribe();
					self.currentUser = user;
					resolve(user);
				} else {
					reject();
				}
			});
		});
	}

}