export const environment = {
	production: true
};

export const HUNT_FORM = {
	hunt_info: [''],
	char: [{
		name: '',
		lvl: '',
		voc: ''
	}],
	char_name: [''],
	char_lvl: [''],
	char_voc: [''],
};