// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
	production: false
};

export const HUNT_FORM = {
	hunt_info: [`Session data: From 2017-08-24, 23:02:01 to 2017-08-25, 00:15:09
Session: 01:13h
XP Gain: 1,154,478
XP/h: 874,662
Loot: 486,967
Supplies: 76,020
Balance: 410,947
Damage: 639,854
Damage/h: 595,711
Healing: 26,183
Healing/h: 24,628
Killed Monsters:
190x frazzlemaw
34x shock head
105x silencer
Looted items:
53x a great mana potion
31x a great health potion
1x terra legs
1x terra boots
1x a small sapphire
1040x a platinum coin
1x a stealth ring
2x boots of haste
1x a big bone
6x iron ore
9x a fish fin
9x a hardened bone
49x an assassin star
2x a diamond sceptre
1x an assassin dagger
6x a haunted blade
2x a titan axe
2x a nightmare blade
3x a gold ingot
4x sai
6x a violet crystal shard
21x a brown crystal splinter
6x a red crystal fragment
1x a cluster of solace
30x a frazzle tongue
31x a frazzle skin
15x silencer claws
5x a silencer resonating chamber`],
	char: [{
		name: 'Lady Death Star',
		lvl: '205',
		voc: 'ek'
	}],
	char_name: ['Lady Death Star'],
	char_lvl: ['205'],
	char_voc: ['ek'],
};