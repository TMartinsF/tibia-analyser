import { Route } from '@angular/router';

import { HuntComponent } from '../pages/hunt/hunt.component';
import { LoginComponent } from '../pages/login/login.component';
import { MainComponent } from '../pages/main/main.component';
import { NotFoundComponent } from '../pages/404/404.component';

/**
 * Define application routes for pages without authentication
 * Must be imported at app.module
 */
export const APP_ROUTES: Route[] = [
	{ path: '', redirectTo: 'main', pathMatch: 'full' },
	{ path: 'hunt', component: HuntComponent },
	{ path: 'login', component: LoginComponent },
	{ path: 'main', component: MainComponent },
	{ path: '404', component: NotFoundComponent },
	{ path: '**', redirectTo: '/404' }
];

/**
 * Define all components for driver module
 * This is set at driver.module declarations
 */
export const APP_MODULE_COMPONENTS = [
	HuntComponent,
	LoginComponent,
	MainComponent,
	NotFoundComponent
];