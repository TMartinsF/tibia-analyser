// Angular
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterializeModule } from 'angular2-materialize';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

// Firebase
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireModule } from 'angularfire2';
import { AngularFirestoreModule } from 'angularfire2/firestore';

// Paste in your credentials that you saved earlier
const firebaseConfig = {
	apiKey: 'AIzaSyDT52ybIN-Vuq-H6K9BrzVz039PI6OkXjE',
	authDomain: 'tibia-7.firebaseapp.com',
	databaseURL: 'https://tibia-7.firebaseio.com',
	projectId: 'tibia-7',
	storageBucket: 'tibia-7.appspot.com',
	messagingSenderId: '296465472687'
};

// Providers
import { AuthService } from '../providers/auth.service';
import { HuntService } from '../pages/hunt/hunt.service';

// Routes
import { APP_ROUTES, APP_MODULE_COMPONENTS } from './app.routes';

// Rxjs
import '../util/rxjs-extensions';

// Components
import { AppComponent } from './app.component';

@NgModule({
	declarations: [
		AppComponent,
		APP_MODULE_COMPONENTS
	],
	imports: [
		AngularFireModule.initializeApp(firebaseConfig),
		AngularFirestoreModule,
		BrowserModule,
		FormsModule,
		MaterializeModule,
		ReactiveFormsModule,
		RouterModule.forRoot(APP_ROUTES)
	],
	providers: [
		AuthService,
		AngularFireAuth,
		HuntService,
	],
	bootstrap: [AppComponent]
})
export class AppModule { }
