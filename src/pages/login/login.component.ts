import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, Validators, ReactiveFormsModule } from '@angular/forms';

import { AuthService } from '../../providers/auth.service';

import swal from 'sweetalert2';

// Validators
import { EmailValidator } from '../../validators/email.validator';

// declare var firebase;

@Component({
	selector: 'login-page',
	templateUrl: './login.component.html',
	styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

	formLogin: any;
	errorMessage = '';
	user: any;
	loaded = false;

	constructor(
		public _authService: AuthService,
		public formBuilder: FormBuilder,
		public router: Router
	) { }

	ngOnInit(): void {
		const self = this;

		self._authService.isLoggedIn().then((success) => {
			self.router.navigate(['/main']);
			console.log('logged in');
			console.log(success);
		}, (error) => {
			self.loaded = true;
			console.log('not logged in');
			console.log(error);
		});

		self.formLogin = self.formBuilder.group({
			email: ['linkthales@hotmail.com', Validators.compose([Validators.required, EmailValidator.isValid])],
			password: ['123456', Validators.compose([Validators.required, Validators.minLength(6)])]
		});
	}

	// Realiza login convencional
	login() {
		console.log('login()');

		this._authService.loginWithPassword(this.formLogin.value).then(
			(success) => {
				console.log(success);
				this.router.navigate(['/main']);
				swal('Login', 'Entrou.', 'success');
			}, (error) => {
				// Handle Errors here.
				switch (error.code) {
					case 'auth/invalid-email': {
						swal('Falha no login', 'E-mail inválido.', 'error');
						break;
					}
					case 'auth/wrong-password': {
						swal('Falha no login', 'Senha incorreta.', 'error');
						break;
					}
					case 'auth/user-not-found': {
						swal('Falha no login', 'E-mail não encontrado.', 'error');
						break;
					}
					case 'auth/user-disabled': {
						swal('Falha no login', 'Conta está desabilitada', 'error');
						break;
					}
					default: {
						swal('Falha no login', 'Tente novamente mais tarde.', 'error');
					}
				}
			});
	}

	create() {
		// firebase.auth().createUserWithEmailAndPassword("linkthales@hotmail.com", "password").then(
		// 	(success) => {
		// 		swal('Cadastro concluído', 'E-mail cadastrado com sucesso.', 'success');
		// 	}, (error) => {
		// 		// Handle Errors here.
		// 		switch (error.code) {
		// 			case 'auth/email-already-in-use': {
		// 				swal('Falha no cadastro', 'E-mail já cadastrado.', 'error');
		// 				break;
		// 			}
		// 			case 'auth/invalid-email': {
		// 				swal('Falha no cadastro', 'E-mail inválido.', 'error');
		// 				break;
		// 			}
		// 			case 'auth/operation-not-allowed': {
		// 				swal('Falha no cadastro', 'Operação não permitida.', 'error');
		// 				break;
		// 			}
		// 			case 'auth/weak-password': {
		// 				swal('Falha no cadastro', 'Senha muito fraca.', 'error');
		// 				break;
		// 			}
		// 			default: {
		// 				swal('Falha no cadastro', 'Tente novamente mais tarde.', 'error');
		// 			}
		// 		}
		// 	});
	}

	// Realiza login via Facebook
	facebookLogin() {
		const self = this;

		self._authService.facebookLogin().then((success) => {
			console.log(success);
			this.router.navigate(['/main']);
		}, (error) => {
			console.log(error);
		});
	}

	forgotPassword() {
		swal({
			title: 'Recuperar senha',
			input: 'text',
			html: '<div style="padding:10px;">Digite o e-mail que deseja recuperar a senha.</div>',
			showCancelButton: true,
			inputClass: null,
			inputPlaceholder: 'E-mail',
			cancelButtonText: 'Cancelar',
			confirmButtonText: 'Enviar e-mail de recuperação.',
			reverseButtons: true,
			showLoaderOnConfirm: true,
			// preConfirm: function (email) {
			// 	return new Promise(function (resolve, reject) {
			// 	})
			// },
			allowOutsideClick: false
			// }).then(function (result) {
		});
	}

}
