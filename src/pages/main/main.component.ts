// Angular
import { Component } from '@angular/core';
import { Router } from '@angular/router';

// Providers
import { AuthService } from '../../providers/auth.service';

@Component({
	selector: 'main-page',
	templateUrl: './main.component.html',
	styleUrls: ['./main.component.scss']
})
export class MainComponent {
	title = 'main';

	constructor(
		public _auth: AuthService,
		public router: Router
	) { }

	logout(): void {
		const self = this;
		self._auth.logout().then((success) => {
			console.log(success);
			self.router.navigate(['/login']);
		}, (error) => {
			console.log(error);
		});
	}
}