import { Character } from '../../models/models';

export class Hunt {
	public Balance: string;
	public Damage: string;
	public Damage_h: string;
	public Healing: string;
	public Healing_h: string;
	public Items: any;
	public Loot: string;
	public Monsters: any;
	public Character: Character;
	public Session: string;
	public Sessiondata: string;
	public Supplies: string;
	public XPGain: string;
	public XP_h: string;
}