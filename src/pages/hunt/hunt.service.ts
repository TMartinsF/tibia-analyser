// Angular
import { Injectable } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';

// Firebase
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from 'angularfire2/firestore';

// Models
import { Character, Hunt } from '../../models/models';

// Observables
import { Observable } from 'rxjs/Observable';

// Providers
import { HUNT_FORM } from './../../environments/environment';

// SweetAlert2
import swal from 'sweetalert2';

@Injectable()
export class HuntService {

	private hunt: Array<Hunt>;

	constructor(
		private afStore: AngularFirestore,
		private formBuilder: FormBuilder
	) { }

	addChar(huntForm: FormGroup[]): FormGroup[] {
		const self = this;

		huntForm.push(
			self.formBuilder.group(HUNT_FORM)
		);

		return huntForm;
	}

	removeChar(huntForm: FormGroup[]): FormGroup[] {
		huntForm.pop();
		return huntForm;
	}

	updateFields(huntForm: FormGroup): void {
		huntForm.controls['char_name'].setValue(huntForm.controls.char.value.name);
		huntForm.controls['char_lvl'].setValue(huntForm.controls.char.value.lvl);
		huntForm.controls['char_voc'].setValue(huntForm.controls.char.value.voc);
	}

	onSubmit(huntName: string, huntForm: FormGroup[]): void {
		// 	// console.log(this.huntForm);
		const self = this;
		const huntsArray: Array<string> = [];
		self.hunt = [];

		if (huntForm[0].valid && huntName.length !== 0) {
			// console.log(huntForm[0].value);

			// for (let i = 0; i < huntForm.length; i++) {
			// 	if (huntForm[i].controls['hunt_info'].value) {
			// 		huntsArray.push(huntForm[i].controls['hunt_info'].value);
			// 		console.log('HuntForm Controls ->', huntForm[i].controls);
			// 	}
			// }
			// console.log("Hunts Array ->", huntsArray);

			for (const form of huntForm) {
				if (form.controls['hunt_info'].value.length > 0) {
					self.hunt.push(self.createCharacterHunt(form));
				}
			}

			let balance = 0;
			for (const hunt of self.hunt) {
				balance += Number(hunt.Balance);
				// console.log("Char Balance ->", hunt.Balance);
			}

			const share = balance / self.hunt.length;
			if (balance > 0) {
				// console.log("Profit ->", balance.toFixed(0) + "k");
				// console.log(share.toFixed(0) + "k de profit para cada");
			} else {
				// console.log("Waste ->", balance.toFixed(0) + "k");
				// console.log(share.toFixed(0) + "k de waste para cada");
			}

			let result = '';
			if (self.hunt.length > 1) {
				result = share.toFixed(0) + 'k de lucro para cada jogador<br><br>';
				for (const hunt of self.hunt) {
					const pBalance = Number(hunt.Supplies) + (share);
					result += hunt.Character.name + ' recebe ' + pBalance.toFixed(0) + 'k <br>';
					// console.log(hunt.Character.name + " recebe " + pBalance.toFixed(0) + "k");
				}
			} else {
				result = Number(self.hunt[0].Loot).toFixed(0) + 'k de loot<br><br>';
			}
			swal(huntName, result);

			// console.log("HuntName ->", huntName);
			// console.log("Hunt ->", self.hunt);

			// firebase.database().ref('/hunts/' + huntName + '/').push(self.hunt).then((success) => {
			// 	console.log(success);
			// 	huntForm = [];
			// 	self.addChar();
			// });
		} else {
			swal(
				'Oops...',
				'Campos vazios no formulário!',
				'error'
			);
		}
	}

	createCharacterHunt(huntForm: FormGroup): Hunt {
		const separator = '\n';
		const huntAnalyser = huntForm.controls['hunt_info'].value;
		const huntLine = huntAnalyser.split(separator);
		let counter = 0;

		const huntObj: Hunt = new Hunt();
		const monstersObj = {};
		const itemsObj = {};

		const char: Character = {
			name: huntForm.controls['char_name'].value,
			lvl: huntForm.controls['char_lvl'].value,
			voc: huntForm.controls['char_voc'].value
		};

		huntObj.Character = char;

		for (let i = 0; i < huntLine.length; i++) {
			// console.log(huntLine[i]);
			const linePiece = huntLine[i].split(': ');
			let key = linePiece[0];
			if (key === 'Killed Monsters:') {
				counter = ++i;
				break;
			}
			key = key.replace('\/', '_');
			key = key.replace(' ', '');
			huntObj[key] = linePiece[1].replace('h', '').replace(',', '.');
		}

		// console.log("-------------------- Monsters Start! --------------------");

		for (let j = counter; j < huntLine.length; j++) {
			if (huntLine[j] === 'Looted items:') {
				// console.log("-------------------- Monsters End! --------------------");
				// console.log("-------------------- Items Start! --------------------");
				counter = ++j;
				break;
			}
			const linePiece = huntLine[j].split('x ');
			monstersObj[linePiece[1]] = +linePiece[0].trim();
			// console.log(huntLine[j]);
		}

		for (let k = counter; k < huntLine.length; k++) {
			let linePiece = '';
			if (huntLine[k].includes('None')) {
				break;
			}
			if (huntLine[k].includes('x an ')) {
				linePiece = huntLine[k].split('x an ');
			} else if (huntLine[k].includes('x a ')) {
				linePiece = huntLine[k].split('x a ');
			} else {
				linePiece = huntLine[k].split('x ');
			}
			itemsObj[linePiece[1]] = +linePiece[0].trim();
			// console.log(huntLine[k]);
		}
		// console.log("-------------------- Items End! --------------------");

		// console.log("Person Node ->", huntObj);
		// console.log("Monsters Obj ->", monstersObj);
		// console.log("Items Obj ->", itemsObj);

		huntObj['Monsters'] = Object.keys(monstersObj).length ? monstersObj : 'Nenhum monstro morto.';
		huntObj['Items'] = Object.keys(itemsObj).length ? itemsObj : 'Nenhum loot coletado.';

		return huntObj;
	}

	loadUserChars(user: any): Observable<Character[]> {
		const self = this;

		let charactersRef: AngularFirestoreCollection<Character>;
		console.log('users/' + user.uid + '/characters');
		charactersRef = self.afStore.collection('users/' + user.uid + '/characters');
		// console.log(charactersRef);

		return charactersRef.valueChanges();
	}

}