// Angular
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

// Firebase
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from 'angularfire2/firestore';

// Models
import { Character, Hunt } from './../../models/models';

// Providers
import { AuthService } from './../../providers/auth.service';
import { HuntService } from './hunt.service';
import { HUNT_FORM } from './../../environments/environment';

// SweetAlert2
import swal from 'sweetalert2';

// Validators
import { EmailValidator } from '../../validators/email.validator';
import { Observable } from 'rxjs/Observable';

@Component({
	selector: 'hunt-page',
	templateUrl: './hunt.component.html',
	styleUrls: ['./hunt.component.scss']
})
export class HuntComponent implements OnInit {
	private user: any = null;
	private counter = 1;
	private hunt: Array<Hunt> = [];
	private huntForm: FormGroup[] = [];
	private charObser: Observable<Character[]>;
	private characters: Character[];
	private huntName = 'Lulu';

	constructor(
		private _authService: AuthService,
		private _huntService: HuntService,
		private formBuilder: FormBuilder
	) {
		// console.log("HuntComponentConstructor");
	}

	ngOnInit() {
		const self = this;

		self._authService.isLoggedIn().then((success) => {
			self.user = success;
			console.log('Usuário autenticado ->', self.user.uid);
			self.charObser = self._huntService.loadUserChars(self.user);
			self.charObser.subscribe((characters) => self.characters = characters);
			// console.log(self.charObser);
		}, (error) => {
			console.log('Usuário não autenticado.');
		});
		self.addChar();
	}

	addChar(): void {
		this.huntForm = this._huntService.addChar(this.huntForm);
	}

	removeChar(): void {
		this.huntForm = this._huntService.removeChar(this.huntForm);
	}

	updateFields(index): void {
		this._huntService.updateFields(this.huntForm[index]);
	}

	onSubmit(): void {
		this._huntService.onSubmit(this.huntName, this.huntForm);
	}
}
