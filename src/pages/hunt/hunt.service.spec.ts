import { async, getTestBed, TestBed, inject } from '@angular/core/testing';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import '../../util/rxjs-extensions';

import { AngularFirestore } from 'angularfire2/firestore';
import { HuntService } from './hunt.service';
import { Character } from '../../models/models';

describe('HuntService', () => {
	let _huntService: HuntService;
	const huntForm: FormGroup[] = [];

	const valueChangesSpy: jasmine.Spy = jasmine.createSpy('valueChanges').and.callFake(() => {
		return Observable.of([
			{
				lvl: 207,
				name: 'Yuri Nakamura',
				voc: 'ed'
			},
			{
				lvl: 150,
				name: 'Drannothy',
				voc: 'rp'
			}
		]);
	});

	const collectionSpy: jasmine.Spy = jasmine.createSpy('collection').and.returnValue({
		valueChanges: valueChangesSpy
	});

	const afStore: any = {
		collection: collectionSpy
	};

	beforeEach(() => {
		TestBed.configureTestingModule({
			providers: [
				FormBuilder,
				HuntService,
				{ provide: AngularFirestore, useValue: afStore },
			],
		});
	});

	beforeEach(inject([HuntService], (huntServiceInjected: HuntService) => {
		_huntService = huntServiceInjected;
	}));

	it('should return all characters from a user when the specified UID exists', () => {
		_huntService.loadUserChars({ uid: 'lulu' }).subscribe((characters: any) => {
			expect(collectionSpy).toHaveBeenCalledWith('users/lulu/characters');
			expect(collectionSpy).toHaveBeenCalledTimes(1);
			expect(valueChangesSpy).toHaveBeenCalled();
			expect(valueChangesSpy).toHaveBeenCalledTimes(1);
			expect(characters[0].lvl).toBe(207);
			expect(characters[0].name).toBe('Yuri Nakamura');
			expect(characters[0].voc).toBe('ed');
			expect(characters[1].lvl).toBe(150);
			expect(characters[1].name).toBe('Drannothy');
			expect(characters[1].voc).toBe('rp');
		});
	});

	it('should add new FormBuilder to FormGroup[]', () => {
		_huntService.addChar(huntForm);
		// expect(_huntService.huntForm)
	});

});
