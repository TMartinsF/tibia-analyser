// firebase deploy --only functions:userDeleted

const functions = require('firebase-functions');
const admin = require('firebase-admin');

admin.initializeApp(functions.config().firebase);

// Delete User data from Database
exports.userDeleted = functions.auth.user().onDelete(event => {
    const user = event.data; // The Firebase user.

    return admin.firestore().doc('users/' + user.uid).delete().then((success) => {
        console.log("Dados do usuário: " + user.email + " apagados com sucesso do Firestore.");
    }, (error) => {
        console.log(error);
        console.log("Não foi possível apagar os dados do usuário: " + user.email + " do Firestore.");
    });
});